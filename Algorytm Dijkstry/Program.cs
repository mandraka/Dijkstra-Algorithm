﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;

namespace Algorytm_Dijkstry
{
    class Program
    {
        public class Edge
        {
            public string source;
            public string target;
            public int weight;
            public Edge(string s, string t, int w)
            {
                source = s;
                target = t;
                weight = w;
            }
        }
        public class Node
        {
            public string id;
            public int value;
            public string path;
            public bool enabled;

            public Node (string n, int v, string p, bool b)
                {
                id=n;
                value = v;
                path = p;
                enabled = b;
                }

            public List<Edge> edges = new List<Edge>();

        }
        static void Main(string[] args)
        {
            string a = "", b = "";
            // otworz plik z poleceniem
            string filePath = System.IO.Path.GetFullPath("task.txt");
            using (StreamReader sr = new StreamReader(filePath))
            {
            // wprowadz do dwoch zmiennych
            a = sr.ReadLine();
            b = sr.ReadLine();
            }
            //inicjalizuj listę wierzchołków
            List<Node> nodes = new List<Node>();
            //wczytaj graf
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;
            XmlReader reader = XmlReader.Create("data.xml", settings);
            reader.MoveToContent();
            //wprowadź dane z pliku do listy
            int t = 0;
            while (reader.Read())
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        switch (reader.Name)
                        {
                            case ("graph"):
                                break;
                            case ("node"):
                                Node x = new Node(reader.GetAttribute("id"), Int32.MaxValue, "-", true);
                                nodes.Add(x);
                                break;
                            case ("edge"):
                                Int32.TryParse(reader.GetAttribute("weight"), out t);
                                 Edge e = new Edge(reader.GetAttribute("source"), reader.GetAttribute("target"), t);
                                foreach (Node k in nodes)
                                    if (reader.GetAttribute("source") == k.id )
                                        k.edges.Add(e);
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
            Node current;

            //licz od pierwszego wierzchołka podanego w pliku
            current= nodes.Find(x => x.id.Contains(a));
            current.value = 0;
            current.enabled = false;
            foreach (Edge e in current.edges)
            {
                foreach (Node n in nodes)
                {
                    if ((n.id==e.target||n.id==e.source)&&n.enabled==true)
                        if (e.weight + current.value < n.value)
                        {
                            n.value = e.weight + current.value;
                            n.path = current.id;
                         }
                }
            }
            while (current != nodes.Find(x => x.id.Contains(b)))
            {
                //szukaj nastepnego wierzchołka z najmniejszą wartością
                int lowestValue = Int32.MaxValue;
                foreach (Node n in nodes)
                    if (n.value < lowestValue && n.enabled == true)
                    {
                        lowestValue = n.value;
                        current = n;
                    }
                //licz go
                current.enabled = false;
                foreach (Edge e in current.edges)
                {
                    foreach (Node n in nodes)
                    {
                        if ((n.id == e.target || n.id == e.source) && n.enabled == true)
                            if (e.weight + current.value < n.value)
                            {
                                n.value = e.weight + current.value;
                                n.path = current.id;
                            }
                    }
                }
            }

            // wyświetl ładnie w konsoli wyliczone wierzchołki 
            Console.WriteLine();
            foreach (Node k in nodes)
            {
                Console.WriteLine("Node ID: "+ k.id);
                if (k.value != 2147483647)
                    Console.WriteLine("Distance is: " + k.value);
                else
                    Console.WriteLine("Distance was not calculated due to finish point being already calculated- therefore algorithm breaks");
                
                Console.Write("Path is: ");
                string tempPath=k.path;
                    while (tempPath!="-")
                    {
                    Console.Write(tempPath + " ");
                        foreach (Node m in nodes)
                        {
                            if (m.id==tempPath)
                            {
                            tempPath = m.path;
                            break;
                            }
                        }
                    }
                Console.WriteLine();
                Console.WriteLine("Outward edges are:");
                    
                foreach (Edge e in k.edges)
                    Console.WriteLine(e.source + " " + e.target + " " + e.weight);
                Console.WriteLine();
            }

            //zapisz do pliku result.txt najkrótszą ścieżkę z podanych a i b i jej długość
            using (StreamWriter writer = new StreamWriter("result.txt"))
            {
                writer.WriteLine("Dystans najkrótszej ścieżki: ");
                writer.WriteLine(current.value);
                writer.WriteLine("Najkrótsza ścieżka to: ");
                writer.WriteLine(current.id);
                Node EndNode;
                EndNode = nodes.Find(x => x.id.Contains(a));
                while (current != EndNode)
                {
                    writer.WriteLine(current.path);
                    foreach (Node n in nodes)
                    {
                        if (n.id == current.path)
                        {
                            current = n;
                            break;
                        }
                    }
                }
            }
            Console.ReadKey();
        }
    }
}
