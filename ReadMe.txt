How to use program:

1. In location of main executable create XML file "data.xml" which is graph container
2. Graph template is below:
<?xml version="1.0" encoding="utf-8"?>
<graphml xmlns="http://blank.pl">
  <graph id="G">
    <node id="S" />
    <node id="A" />
	<node id="B" />
	<node id="C" />
	<node id="D" />
	<node id="F" />
	<node id="G" />
	<node id="H" />
	<node id="I" />
	<node id="J" />
	<node id="K" />
	<node id="L" />
	<node id="E" />
    <edge source="S" target="A" weight="7" />
	<edge source="S" target="B" weight="2" />
	<edge source="S" target="C" weight="3" />
	<edge source="A" target="D" weight="4" />
    <edge source="A" target="B" weight="3" />
	<edge source="B" target="D" weight="4" />
	<edge source="B" target="H" weight="1" />
	<edge source="D" target="F" weight="5" />
	<edge source="F" target="H" weight="3" />
	<edge source="H" target="G" weight="2" />
	<edge source="G" target="E" weight="2" />
	<edge source="C" target="L" weight="2" />
	<edge source="L" target="I" weight="4" />
	<edge source="L" target="J" weight="4" />
	<edge source="I" target="J" weight="6" />
	<edge source="I" target="K" weight="4" />
	<edge source="J" target="K" weight="4" />
	<edge source="K" target="E" weight="5" />
  </graph>
</graphml>
3. Executable also needs file task.txt where 2 nodes ID are present in separate lines, first one is start point, last is finish.
4. Program executes in console giving each node quickest path but also produces file result.txt with specified point path and summarised weight.